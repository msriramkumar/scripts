# steamcmd

A simple wrapper script for a containerized steamcmd.

## Credits

Containerfile provided by [CM2Walki](https://github.com/CM2Walki/steamcmd)

## Getting started

1. Build container image using the Containerfile provided.
2. Run the script.

## Notes

This script is only useful if the intended application being downloaded is **DRM free**.
Applications requiring the Steam Client will **fail** to open.

To check if an application is DRM free, please consult [Steam Fandom](https://steam.fandom.com/wiki/List_of_DRM-free_games)
