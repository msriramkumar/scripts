#!/bin/bash

export HOST_INSTALL_PATH="/media/Stuff/SteamLibrary"
export CONTAINER_INSTALL_PATH="/home/steam/games"
export IMAGE_NAME="steam"

read -p "Enter username: " userID
read -p "Enter appID of game: " appid
read -p "(Optional) Enter label: " label

[ -z "$label" ] && label="$appid"

H_DIR="$HOST_INSTALL_PATH/$label"
C_DIR="$CONTAINER_INSTALL_PATH/$label"

#If the host directory does not exist when a volume is mounted, the docker daemon (which runs as root) will create the directory, and thus the folder's owner will be root. By creating the folder beforehand, the docker daemon will simply use that folder, thus no NEW folder is created, and thus the owner will be whoever created the folder originally.
mkdir -pv "$H_DIR"

docker run -it -v "$H_DIR:$C_DIR" "$IMAGE_NAME" bash /home/steam/steamcmd/steamcmd.sh +@sSteamCmdForcePlatformType windows +force_install_dir "$C_DIR" +login "$userID" +app_update "$appid" +quit

STATUS="$?"

[ "$STATUS" != "0" ] && rm -rf "$H_DIR"
