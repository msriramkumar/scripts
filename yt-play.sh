#!/bin/bash
#Scrapes a alt-youtube frontend called Invidious and autostarts the video in mpv

#Not all vids have timestamp, therefore breaking this script;
#This function loops over all the lines and for every fourth line (which should contain the timestamp), checks whether it is actually present and if not, inserts a placeholder;
function processresults(){
  IFS=$'\n' results=($results);
  shiftfactor=1;

  for line in ${!results[@]}; do
    linenum=$((line + shiftfactor));
    if [ $((linenum % 4)) -eq 0 ]; then
      count=$(echo "${results[line]}" | grep -c '<p class="video-data" dir="auto">Shared.*</p>');
      if [ $count -eq 0 ];then
        results[line]=$(echo -e "No timestamp\n${results[line]}")
        ((shiftfactor++))
      fi
    fi
  done;

  IFS=$'\n' results="${results[*]}"
}

function play(){
  limit=20 #Max is 20
  domain="https://yewtu.be" #Any Invidious instance should work

  query="$(dmenu -p 'Search youtube:' <&- | sed 's/ /\+/g')";

  [ -z "$query" ] && exit 1;

  url="$domain/search?type=video&sort=relevance&q=$query";

  results=$(curl -s "$url" | grep -E 'p dir="auto"|title="Watch on YouTube"|class="channel-name"|<p class="video-data" dir="auto">Shared.*</p>');

  processresults

  reltime=$(echo "$results" | awk 'NR % 4 == 0 { print }'| sed -e 's/<p class="video-data" dir="auto">Shared//g;s/<\/p>//g' | head -n "$limit");
  titles=$(echo "$results" | awk 'NR % 4 == 1 { print }'| sed -e 's/<p dir="auto">//g;s/<\/p>//g' | head -n "$limit");
  channelnames=$(echo "$results" | awk 'NR % 4 == 2 { print }'| sed -e 's/<p class="channel-name" dir="auto">//g;s/<\/p>//g;s/&nbsp;<i class="icon ion ion-md-checkmark-circle"><\/i>//g' | head -n "$limit");
  vIDs=$(echo "$results" | awk 'NR % 4 == 3 { print }'| grep -Eo "/watch\?v=.{11}" | head -n "$limit" );

  IFS=$'\n' v=($vIDs);
  IFS=$'\n' t=($titles);
  IFS=$'\n' c=($channelnames);
  IFS=$'\n' rt=($reltime);

  list=();
  for i in "${!t[@]}"; do
    trimmedtitle=$(echo "${t[$i]}" | xargs);
    trimmedchannel=$(echo "${c[$i]}" | xargs);
    trimmedtime=$(echo "${rt[$i]}" | xargs);
    if [ "$trimmedtime" = "No timestamp" ];then
      trimmedtime="";
    else
      trimmedtime=" [$trimmedtime]";
    fi;

    list+=("$trimmedtime $trimmedchannel  ->  $trimmedtitle");
  done

  list+=("Search...");

  while true; do
    IFS=$'\n' selected=($(echo "${list[*]}" | dmenu -i -l "$(($limit + 1))"))

    [ -z "$selected" ] && exit 1;

    playlist=();
    for title in "${selected[@]}"; do

      [ "$title" = "Search..." ] && return 1;

      for i in "${!list[@]}"; do
        if [ "${list[$i]}" = "$title" ];then
          vURL="$domain${v[$i]}&local=true";
          echo "$vURL" | tr -d '\n' | xclip -selection c;
          playlist+=("$vURL");
          break;
        fi
      done
    done

    mpv "${playlist[@]}";

  done

  return 0
}

while true; do
  play;
  [ $? -eq 0 ] && break;
done
