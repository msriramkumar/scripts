#!/bin/bash
# Send a notification when battery falls below a given percentage

# Notify when battery reaches this percentage
threshold=10

# How often to check battery status
sleep_for="5m"

while true; do
  status=$(acpi -b);
  battery_level=$(echo "$status" | cut -d, -f2 | sed s/%//);
  is_charging=$(echo "$status" | grep -c "Charging");

  if [ "$battery_level" -le "$threshold" ] && [ "$is_charging" -eq "0" ];then
    notify-send "⚠️  Low battery: ${battery_level}%" "Recharge now!" -u critical -t 10000
  fi

  sleep "$sleep_for"
done
